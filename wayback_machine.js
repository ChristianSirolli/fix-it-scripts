location.href = 'https://web.archive.org/web/*/'+(!!location.href.match(/chrome-error:/g) ? 
    (document.getElementById('reload-button').url ?
        document.getElementById('reload-button').url
        : document.getElementsByTagName("strong")[0].innerText)
    : location.href);

// Go Wayback - Bookmarklet
// javascript:(function(){location.href='https://web.archive.org/web/*/'+(!!location.href.match(/chrome-error:/g)?(document.getElementById('reload-button').url?document.getElementById('reload-button').url:document.getElementsByTagName("strong")[0].innerText):location.href);})();