// From: https://digwp.com/2011/07/clean-up-weird-characters-in-database/
// â€œ = left quote = “
// â€ = right quote = ”

// â€˜ = left single quote = ‘
// â€™ = right single quote = ’

// â€” = en dash = –
// â€“ = em dash = —

// â€¢ = hyphen = -
// â€¦ = ellipsis = …
var body = document.getElementsByTagName('body')[0],
    weirdChars = {
	"â€œ":"“",
	"â€":"”",
	"â€˜":"‘",
	"â€™":"’",
	"â€”":"–",
	"â€“":"—",
	"â€¢":"-",
	"â€¦":"…",
};
Object.keys(weirdChars).forEach(function(key) {
	body.innerHTML = body.innerHTML.replace(new RegExp(key, 'g'), weirdChars[key]);
});
// Fix Weird Characters - Bookmarklet
// javascript:(function(){var body=document.getElementsByTagName('body')[0],weirdChars={"â€œ":"“","â€":"”","â€˜":"‘","â€™":"’","â€”":"–","â€“":"—","â€¢":"-","â€¦":"…",};Object.keys(weirdChars).forEach(function(key){body.innerHTML=body.innerHTML.replace(new RegExp(key,'g'),weirdChars[key]);});})();