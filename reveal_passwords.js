// Source: https://superuser.com/q/529859
function text() {
    this.type = "text";
}
function password() {
    this.type = "password";
}
function addHandlers() {
    var e = false;
    var t = document.evaluate("//input[@type='password']", document, null, 6, null);
    for (var n = t.snapshotLength - 1, r; r = t.snapshotItem(n); n--) {
        r.addEventListener(!e?"mouseover":"focus", text, false);
        r.addEventListener(!e?"mouseout":"blur", password, false);
    }
}
addHandlers();
// Hover & Show Password - Bookmarklet
// javascript:(function(){function text(){this.type="text";}function password(){this.type="password";}function addHandlers(){var e=false;var t=document.evaluate("//input[@type='password']",document,null,6,null);for(var n=t.snapshotLength-1,r;r=t.snapshotItem(n);n--){r.addEventListener(!e?"mouseover":"focus",text,false);r.addEventListener(!e?"mouseout":"blur",password,false);}}addHandlers();})();